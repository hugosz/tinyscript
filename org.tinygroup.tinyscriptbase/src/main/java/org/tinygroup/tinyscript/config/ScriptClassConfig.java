package org.tinygroup.tinyscript.config;

import java.util.List;

/**
 * 脚本类的定义项
 * @author yancheng11334
 *
 */
public interface ScriptClassConfig {

	String getScriptClassName();
	
	List<FunctionConfig> getFunctions();
}
