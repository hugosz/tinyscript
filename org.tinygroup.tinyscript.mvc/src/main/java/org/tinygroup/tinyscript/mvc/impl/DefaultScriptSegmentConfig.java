package org.tinygroup.tinyscript.mvc.impl;

import java.util.ArrayList;
import java.util.List;

import org.tinygroup.tinyscript.ScriptClassMethod;
import org.tinygroup.tinyscript.ScriptSegment;
import org.tinygroup.tinyscript.config.FunctionConfig;
import org.tinygroup.tinyscript.config.ScriptClassConfig;

public class DefaultScriptSegmentConfig implements ScriptClassConfig{

	private String className;
	private List<FunctionConfig> functions = new ArrayList<FunctionConfig>();
	
	public DefaultScriptSegmentConfig(ScriptSegment segment){
		className = segment.getScriptClass().getClassName();
		if(segment.getScriptClass().getScriptMethods()!=null){
		   for(ScriptClassMethod method:segment.getScriptClass().getScriptMethods()){
			  functions.add(new ScriptClassMethodConfig(method));
		   }
		}
		
	}
	
	public String getScriptClassName() {
		return className;
	}

	public List<FunctionConfig> getFunctions() {
		return functions;
	}

}
