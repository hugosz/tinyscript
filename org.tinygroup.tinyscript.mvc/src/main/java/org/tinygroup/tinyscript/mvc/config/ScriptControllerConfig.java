package org.tinygroup.tinyscript.mvc.config;

import org.tinygroup.tinyscript.config.FunctionConfig;
import org.tinygroup.tinyscript.mvc.RequestMapping;
import org.tinygroup.tinyscript.mvc.ScriptController;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("controller")
public class ScriptControllerConfig implements ScriptController{

	@XStreamAlias("request-mapping")
	private RequestMappingConfig  requestMapping;
	
	@XStreamAsAttribute
	@XStreamAlias("class-name")
	private String scriptClassName;
	
	@XStreamAsAttribute
	@XStreamAlias("function-name")
	private String functionName;

	public RequestMapping getRequestMapping() {
		return requestMapping;
	}

	public String getScriptClassName() {
		return scriptClassName;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setRequestMapping(RequestMappingConfig requestMapping) {
		this.requestMapping = requestMapping;
	}

	public void setScriptClassName(String scriptClassName) {
		this.scriptClassName = scriptClassName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getUrl() {
		if(requestMapping!=null && requestMapping.getPaths()!=null && requestMapping.getPaths().length>0){
		   return requestMapping.getPaths()[0];
		}
		return null;
	}

	public FunctionConfig getFunctionConfig() {
		return null;
	}
	
}
