package org.tinygroup.tinyscript.mvc;

import org.tinygroup.tinyscript.config.FunctionConfig;

public interface ScriptController {

	/**
	 * 请求相关信息
	 * @return
	 */
	RequestMapping  getRequestMapping();
	
	/**
	 * 脚本类相关信息
	 * @return
	 */
	String  getScriptClassName();
	
	/**
	 * 获得函数名
	 * @return
	 */
	String  getFunctionName();
	
	/**
	 * 获取路径
	 * @return
	 */
	String  getUrl();
	
	/**
	 * 函数相关信息
	 * @return
	 */
	FunctionConfig getFunctionConfig();
	
	
}
